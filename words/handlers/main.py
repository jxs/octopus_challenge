import ast
import base64
from operator import itemgetter

from tornado import web, httpclient
from bs4 import BeautifulSoup

from Crypto.Hash import SHA256
from Crypto.Cipher import PKCS1_OAEP

from .utils import extract_words


class MainHandler(web.RequestHandler):

    def initialize(self, db, key):
        self.db = db
        self.key = key

    def get(self):
        self.render('main.html', url=None, errors=None)

    async def post(self):
        url = self.get_body_argument('url')
        http_client = httpclient.AsyncHTTPClient()
        try:
            response = await http_client.fetch(url)
        except Exception as e:
            self.render("main.html", errors=e, words="", url=url)

        soup = BeautifulSoup(response.body, 'html.parser')
        for script in soup(['script', 'style']):
            script.extract()
        text = soup.get_text()

        # sort and get 100 most frequent words from extract_words function
        words = sorted(extract_words(text).items(), key=itemgetter(1), reverse=True)[:100]
        encryptor = PKCS1_OAEP.new(self.key)
        words_db = [(SHA256.new(w[0].encode('utf-8')).hexdigest(),
                     base64.encodestring(encryptor.encrypt(w[0].encode())).decode(),
                     w[1]) for w in words]
        conn = await self.db.acquire()

        query = """INSERT INTO words (hash, encrypted, frequency) VALUES {}
        ON DUPLICATE KEY UPDATE frequency = frequency + VALUES(frequency);""" .format(','.join(map(str, words_db)))

        cursor = await conn.cursor()
        await cursor.execute(query)
        await conn.commit()

        self.db.release(conn)

        # convert to d3 format
        words_d3 = [{'text': key, 'size': val} for key, val in words]
        self.render("main.html", words=words_d3, url=url, errors=None)
