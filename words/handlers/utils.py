import re

INVALID_WORDS = [
    'a',
    'an'
    'it',
    'with',
    'at',
    'from',
    'into',
    'during',
    'including',
    'until',
    'against',
    'among',
    'throughout',
    'despite',
    'towards',
    'upon',
    'concerning',
    'of',
    'to',
    'in',
    'for',
    'on',
    'by',
    'about',
    'like',
    'through',
    'over',
    'before',
    'between',
    'after',
    'since',
    'without',
    'under',
    'within',
    'along',
    'following',
    'across',
    'behind',
    'beyond',
    'plus',
    'except',
    'but',
    'up',
    'out',
    'around',
    'down',
    'off',
    'above',
    'near',
    'the',
    'in',
    'a',
    'an'
]


def extract_words(text, remove_camel_cased=False):
    lines = [line.strip() for line in text.splitlines()]
    # remove non word characters
    words = [word.strip() for line in lines for word in line.split(' ')]
    pwords = {}
    # search for camel cased words, bs4 get_text() returns camelcased words
    for word in words:

        word = re.sub(r'(\W|\d)', '', word.lower())
        if not word or word.startswith("http") or word in INVALID_WORDS:
            continue

        found = re.sub(r'(?!^)([A-Z][a-z]+)', r' \1', word).split()

        for f in found:
            if f not in pwords:
                pwords[f] = 1
            else:
                pwords[f] += 1

    return pwords
