import base64

from tornado import web
from tornado_http_auth import DigestAuthMixin, auth_required

from Crypto.Cipher import PKCS1_OAEP

import aiomysql

credentials = {'admin': 'OctopusChallenge17'}

class AdminHandler(DigestAuthMixin, web.RequestHandler):

    def initialize(self, db, key):
        self.db = db
        self.key = key

    @auth_required(realm='Restricted', auth_func=credentials.get)
    async def get(self):
        conn = await self.db.acquire()
        cursor = await conn.cursor()
        await cursor.execute("SELECT encrypted, frequency FROM words ORDER BY frequency DESC")

        result = await cursor.fetchall()
        decryptor = PKCS1_OAEP.new(self.key)
        words = [(decryptor.decrypt(base64.decodestring(w[0].encode())), w[1]) for w in result]

        self.db.release(conn)
        self.render("admin.html", words=words)
