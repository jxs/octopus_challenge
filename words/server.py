import argparse
import os
import asyncio
import logging

from tornado.web import Application
from tornado.httpserver import HTTPServer
from tornado.platform.asyncio import AsyncIOMainLoop
from tornado.log import enable_pretty_logging

from aiomysql import create_pool
from Crypto.PublicKey import RSA

from handlers import MainHandler, AdminHandler

logger = logging.getLogger()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='run Octopus code challenge')
    parser.add_argument('-p', '--port', help='Specifies the HTTP port to run the server on (defaults to 8080)',
                        type=int, default=8080)
    parser.add_argument('-d', '--debug', help='run in debug mode', action='store_true')
    args = parser.parse_args()
    static = (os.path.join(os.path.dirname(__file__), "static"))

    # install asyncio loop
    AsyncIOMainLoop().install()

    db = asyncio.get_event_loop().run_until_complete(
        create_pool(host=os.environ.get('DB_HOST'), user=os.environ.get('DB_USER'),
                    password=os.environ.get('DB_PASS'), db=os.environ.get('DB_NAME')))

    #open RSA keys
    fpub = open('public.pem','r')
    fpriv = open('private.pem', 'r')
    pub = RSA.importKey(fpub.read(), passphrase=os.environ.get("KEY_PASSPHRASE"))
    priv = RSA.importKey(fpriv.read(), passphrase=os.environ.get("KEY_PASSPHRASE"))

    app = Application([
        (r"/", MainHandler, dict(db=db, key=pub)),
        (r"/admin", AdminHandler, dict(db=db, key=priv)),
    ], static_path=static, debug=args.debug)

    enable_pretty_logging()

    logger.info("starting server on port: {}".format(args.port))
    server = HTTPServer(app)
    server.bind(args.port, "127.0.0.1")
    server.start()

    asyncio.get_event_loop().run_forever()
